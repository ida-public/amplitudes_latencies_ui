# ABR_analysis

ABR_analysis is a Matlab Graphical User Interface developed within the Data Acquisition and Signal Processing Facility of the Hearing Institute. 
It allows to vizualize ABR data, automatically detect ABR waves amplitudes and latencies, provides manual curation, propose automatic auditory threshold detection based on an iterative cross-correlation procedure.  
It is compatible with data acquired with the common ABR setup provided in the Animal Facility of the Hearing Institute and with the data acquired by the CERIAH. Is is flexible and new kind of data can also be integrated with some additional script to convert data to standardized data format readable by the app. 

## Requirements
For standalone app (.exe) nothing is required except a windows environment. 

For Matlab users : 
- Matlab 
- Image Processing Toolbox
- Signal Processing Toolbox
- Statistics and Machine Learning Toolbox
- Wavelet Toolbox


## User Interface design and workflow

### Overview
![Interface](/Amplitudes_latencies_UI_resources/ABRanalysis.PNG "Interface")

### Description

There is 4 panels: 
1- Load Button allow to load a .mat file: the name is displayed in the box below and the data automatically pops up.

2- Display Button creates the figure as well but can be modified with 2 options : step size is the distance between two consecutive signal in uV (default is 5uV) and Reverse option (= +/-1) allows you to reverse the signal according to the standardization you are following. 

3- Amplitudes and latencies Button computes the amplitudes and latencies of each of the 5 ABR waves. With the Slider you can go through all the dB levels of you data file. The Wave1> option is set by default to 1ms. It is used as a constraint for the algorithm to find the first ABR wave and corresponds to a typical value for physiological ABR. However if you have abnormal ABR and still want to compute automatically amplitudes and latencies you can modify this parameter. The amplitude of wave I corresponds to the distance between N1 and P1, and same for Wave II to V. It is the same definition if you revert the display, and that N1>P1 in time, the amplitude will still be computed as the distance between N and P of the current wave. 

4- The Auditory threshold button compute a robust and reproducible auditory threshold. Is has two options for the method (Cross correlation or Wavelet) and you can choose the level of confidence of the algorithm. It is under development and might not work properly so it can not be used blindly. In case you systematically observe wrong threshold based on your visual observation, I can integrate these data in finding more accurate algorithms.  

5- Refresh and Save Button to be used each time you manually modify the labelling of the peaks and throughs. 

Results are saved in an excel file with the date of the day. 
Previously saved labeling and computation can be reloaded at any time. 
The app is under development, new features are regularly added. 
In the current state, the button "Temporal Evolution" does not work. 

## Installation
### Option 1: With Matlab 

1- Download the file "ABR_analysis.mlappinstall"

2- Open Matlab 

3- Click Onglet « APPS »

4- Click Install App

5- Find the dowloaded app.

Now you have a new icon that you can just click on to launch the app.

### Option 2: Without Matlab 

1- Download the file ABR_analysis_software.exe in the folder "\ABR_analysis_software\for_redistribution"

2- Execute it and check the option "add shorcut to desktop"

3- Launch the app 

## Usage
This software can not be used blindly. It requires to check outputs and correct bad labeling or under/over estimated auditory threshold.

For CERIAH files, data requires to be preprocessed to be read by the software. You can run in your Matlab editor the script pipeline_human_data.m and then load the data_filter.mat files that should have been created. 

## Support
For any help, you can contact me at : 
clara.dussaux@pasteur.fr


## Contributing
We are open to contributions. Send us an email to discuss how to procede. 

## Authors and acknowledgment
If you use this work for your research, please acknowledge our work:
We acknowledge Clara DUSSAUX of the Hearing Institute Data Acquisition and Neural Signal Processing Facility of the C2RI for the development of the ABRAnalysis user interface. 

## License
Copyright © 2023 Institut de l'Audition - Institut Pasteur
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Project status
The software is currently under development. Edited in July 2023
