function [timedata, h] = read_data(filename)

%%% Code to extract data from .txt files acquired with echosoft
%%% the stimulus starts at 2msec. 
%%% The stimulus is a clic. 
%%% ABR Response belongs to [0 10]ms after stimulus onset. 
%%% The label #Rejected (True/False) is currently not taken into account.
%%% However, we could consider to remove the trace with the label true and
%%% at the end keep the same number of pair/impair buffer which corresponds
%%% to min(length(pair, impair)) to compare the same number in each group.
%%% Now, the threshold at acquisition that gives the true/false rejection
%%% label is fixed at 20uV.

%%% Initial version : CERIAH 
%%% Modified by Clara Dussaux : PF Acquisition et Traitement du Signal IDA.

%clc; close all;

%KeepData = questdlg('Les donn�es sont elle deja Parser','','Oui','Non','modal');
%if strcmp(KeepData, 'Non')
%    clear all;

    %[filename,pathname] = uigetfile('*.txt');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Lecture du fichier                                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %FileId = fopen([pathname filename]);
    FileId = fopen(filename);

    NbBuff=0;
    analog_scale = 6.6; % pleine �chelle du convertisseur analogique.
    bit_scale = 2^16;
    keepdataA = [];
    keepdataB = [];
    % Cela signifie que sur la plage [0 (2^16 - 1)], on passe de 0 � 6.6V.
    % Ou encore, 0bit is 0V and 65535 bit is 6.6V. 
    if FileId > 0
        while ~feof(FileId)
            while(strcmp(fgetl(FileId), '#NbData') ~= 1),end;
            Nsig = sscanf(fgetl(FileId),'%i');
            Nfftsig = 2^(nextpow2(Nsig)-1);

            while(strcmp(fgetl(FileId), '#FreqSample') ~= 1),end;
            Fesig = sscanf(fgetl(FileId),'%i');

            while(strcmp(fgetl(FileId), '#Rejected') ~= 1),end;
            rejection = fgetl(FileId);
            if strcmp(rejection, 'true')
                keep = 0;
            else
                keep = 1;
            end
            keepdataA = [keepdataA keep];
            
            while(strcmp(fgetl(FileId), '#GainChaine') ~= 1),end;
            gain = sscanf(fgetl(FileId),'%i');

            NbBuff = NbBuff+1;
            while(strcmp(fgetl(FileId), '#Data--') ~= 1),end;
            for i=1:Nsig
                tAReadsig(NbBuff,i) = sscanf(fgetl(FileId),'%d');
            end
            tAReadsig(NbBuff,1:Nsig) = (tAReadsig(NbBuff,1:Nsig) - mean(tAReadsig(NbBuff,1:Nsig)))* analog_scale/bit_scale;

            fgetl(FileId); % on recupere la derniere ligne      
            if feof(FileId)
                NbBuff = NbBuff-1;
                tAReadsig = tAReadsig(1:NbBuff,:);
                break
            else
                while(strcmp(fgetl(FileId), '#Rejected') ~= 1),end;
                rejection = fgetl(FileId);
                if strcmp(rejection, 'true')
                    keep = 0;
                else
                    keep = 1;
                end
                keepdataB = [keepdataB keep];

                while(strcmp(fgetl(FileId), '#Data--') ~= 1),end;
                for i=1:Nsig
                    tBReadsig(NbBuff,i) = sscanf(fgetl(FileId),'%d');
                end
                tBReadsig(NbBuff,1:Nsig) = (tBReadsig(NbBuff,1:Nsig) - mean(tBReadsig(NbBuff,1:Nsig)))*  analog_scale/bit_scale;
            end

            fgetl(FileId); % on recupere la derniere ligne      
            disp(NbBuff*2);
        end
    end
    clc;
%end

keepdataA = keepdataA(1:NbBuff);
keepdataB = keepdataB(1:NbBuff);

tAReadsig_filt = tAReadsig(keepdataA==1,:);
tBReadsig_filt = tBReadsig(keepdataB==1,:);
mmin = min(size(tAReadsig_filt,1), size(tBReadsig_filt,1));
tAReadsig_filt = tAReadsig_filt(1:mmin,:);
tBReadsig_filt = tBReadsig_filt(1:mmin,:);

AxeTsig = (1:Nsig)/Fesig; % Axe des temps
AxeFsig = (0:Nfftsig-1)*Fesig/Nfftsig; % Axe des fr�quences
disp(filename);

str = sprintf('Nombre de Data Sig       : %d', Nsig);
disp(str);

str = sprintf('Nombre de Data FFT Sig   : %d', Nfftsig);
disp(str);

NbBuff = mmin;
str = sprintf('Nombre de buffer         : %d', NbBuff*2);
disp(str);

Order = 4; %double passage de 2
Wn = [100 2000]/(Fesig/2); %Frenquence du passe bande
% Ws = [Wp(1)/10 Wp(2)*10]; %Une decade de chaque cot�
% if Ws(2) > (15000/(Fesig/2));
%     Ws(2) = 15000/(Fesig/2);
% end
% Rp = 3; % 3dB cd passeband ripple
% Rs = 80; % 40 dB attenuation equivalent odre 2 dans les 2 sens
% [Order,Wn] = buttord(Wn,Ws,Rp,Rs);
%[C,D] = butter(Order,Wn,'Bandpass');
%FVTool(C,D)

[B,A] = butter(Order/2,Wn,'Bandpass');

% subplot(2,3,1)
% [H1,f1]=freqz(B,A,512,Fesig/2);
% semilogx(f1,20*log(abs(H1))) 
% xlabel('Frequency (Hz) ') 
% ylabel('Magnitude Response')
% axis([10, 10000, -50, 10]);
% subplot(2,3,4)
% zplane(B,A)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Moyennage + Filtrage une fois du totale                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if NbBuff > 1
    tStudy = (sum(tAReadsig_filt(1:NbBuff,1:Nsig)) + sum(tBReadsig_filt(1:NbBuff,1:Nsig))) / NbBuff;
else
    tStudy = tAReadsig_filt(1,1:Nsig) + tBReadsig_filt(1,1:Nsig);
end

tFiltre = filtfilt(B,A,tStudy);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filtrage de toute les trame + Moyennage                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for w=1:NbBuff
    TASigF(w,1:Nsig) = filtfilt(B,A,tAReadsig_filt(w,1:Nsig));
    TBSigF(w,1:Nsig) = filtfilt(B,A,tBReadsig_filt(w,1:Nsig));
end

if NbBuff > 1
    tStudyF = (sum(TASigF(1:NbBuff,1:Nsig)) + sum(TBSigF(1:NbBuff,1:Nsig))) / NbBuff;
else
    tStudyF = TASigF(1,1:Nsig) + TBSigF(1,1:Nsig);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Affichage Courbe                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure
% 
% hold on
%       plot(AxeTsig*1000,tStudy/gain*1e6,'r-');    
%       plot(AxeTsig*1000,tFiltre/gain*1e6,'b-');
%       plot(AxeTsig*1000,tStudyF/gain*1e6,'g-');
% hold off
% title('Allure temporelle moyenne (uV)');
% xlabel('temps (msec)');     
% grid;

% figure
% 
% hold on
%       plot(AxeTsig(AxeTsig<0.02)*1000,tStudy(AxeTsig<0.02)/gain*1e6,'r-');    
%       plot(AxeTsig(AxeTsig<0.02)*1000,tFiltre(AxeTsig<0.02)/gain*1e6,'b-');
%       plot(AxeTsig(AxeTsig<0.02)*1000,tStudyF(AxeTsig<0.02)/gain*1e6,'g-');
% hold off
% title('Allure temporelle moyenne (uV)');
% xlabel('temps (msec)');     
% grid;

timedata = AxeTsig*1000;; % AxeTsig(AxeTsig<0.02)*1000;
h = tStudyF/gain*1e6; % tStudyF(AxeTsig<0.02)/gain*1e6;