function y=bandpass_filt(x,dt,f,df)
% Band-pass filtering of a signal based on the FFT
% f - The frequency interval of the filtering, in kHz if dt is in seconds
% df - A damping parameter specifying the sharpness of the cutoff oustide
% the specified interval
% 

if(nargin<=2) f=[0.3 3]; end
if(nargin<=3) df=f./20; end

if(~isempty(df)&df(2)<inf)
    % Build a bandpass filter centered around frequencies fc=sum(f)/2
    % and with cuttofs specified by df below f(1) and above f(2).
    N=length(x); Fx=fftshift(fft(x));
    f1=1e3*f(1); f2=1e3*f(2); df1=1e3*df(1); df2=1e3*df(2);
    fr=-floor(N./2)./(N*dt):1./(N.*dt):floor((N-1)./2)./(N*dt);
    bpf=2+(fr<f1).*(exp(-(fr-f1).^2./df1.^2)-1)+(fr>f2).*(exp(-(fr-f2).^2./df2.^2)-1)...
        +(fr>-f1).*(exp(-(fr+f1).^2./df1.^2)-1)+(fr<-f2).*(exp(-(fr+f2).^2./df2.^2)-1);

    %Apply the filter to remove frequencies
    if(size(x,1)>size(x,2)) bpf=bpf'; end
    Fy=ifftshift(Fx.*bpf);
    %Go back to real space
    y=real(ifft(Fy));
    %y=ifft(Fy);
else y=x;
end