function output = detect_threshold_wv(dat,system,gain,Ltube,reverse,alpha)

%%% Wavelet transform analysis of ABR for threshold detection

    % 1- Extract data 
    cson = 340; % spee of sound
    risk = alpha; % risk alpha 
    
    % initialisation of ABR wave parameters
    hsgn = reverse; %1 is normal, -1 is in the animal facility visualisation
    
    %get sound level parameters
    %noise levels
    try Lmin_nois = dat.stimparams.Lmin_nois; 
        Lmax_nois = dat.stimparams.Lmax_nois;
        dL_nois   = dat.stimparams.delL_nois; 
    catch 
        Lmin_nois = 0;
        Lmax_nois = 0;
        dL_nois = 0;
    end
    if(dL_nois==0) 
        Lnois=Lmin_nois;
    elseif(dL_nois>0)
        Lnois=Lmin_nois:dL_nois:Lmax_nois;
        Lnois = [0 Lnois];
    elseif(dL_nois<0)
        Lnois=Lmax_nois:dL_nois:Lmin_nois;
        Lnois = [0 Lnois];
    end
    
    Lmin = dat.stimparams.Lmin;
    Lmax = dat.stimparams.Lmax;
    dL = dat.stimparams.delL;
    
    if dL==0
        L = Lmin.*ones(1,length(Lnois));
    elseif(dL>0)
        L = Lmin:dL:Lmax;
        Lnois=Lmin_nois*ones(1,length(L));
    elseif(dL<0)
        L=Lmax:dL:Lmin;
        Lnois=Lmin_nois*ones(1,length(L));
    end
    
    if strcmp(system,'CERIAH')
        L=1:size(dat.hrav,2);
    elseif strcmp(system,'Otophylab')
        L=dat.stimparams.L;
    end

    %1) Filter wave(s) whithin specified band
    fs   = dat.stimparams.fech; 
    t0   = 1e-3.*dat.stimparams.espace./2;
    ttube= Ltube/cson;
    tt   = 1e3*(dat.tt1-t0-ttube);
    hrfac= 1e6./gain;

    try 
        h=hsgn*hrfac.*dat.hrav;
    catch
        h=hsgn*hrfac.*dat.hravsing;
    end

    h    = h-mean(h);
    
    nplots =  size(h,2);
    figure, set(gcf,'position',[125 50 800 100*nplots])
    M = [];
    interval = find(tt>1 & tt<10);
    frequency_range = 0.4; 
    
    for i = 1:nplots
    
        htmp = h(:,i); 
        [cfs,frq] = cwt(htmp,fs);
        
        if strcmp(system,'CERIAH')
            k1 = 2*(nplots-i)+1;
            k2 = 2*(nplots-i+1); 
        else
            k1 = 2*(i-1)+1;
            k2 = 2*i; 
        end
    
        subplot(nplots,2,k1); surface(tt,frq,abs(cfs)); set(gca,"yscale","log")
        axis tight; shading flat; xlabel("Time (s)"); ylabel("Frequency (Hz)")
        hold on; arrayfun(@xline,0, 'w'); arrayfun(@xline,10, 'w'); arrayfun(@yline,frequency_range,'w');
    
        if strcmp(system,'CERIAH') || strcmp(system,'Otophylab')
            % Condition sur la baseline temporelle
            interval_pre = find(tt>10);
            % Condition sur la fréquence
            freq_search = find(frq>frequency_range);
        else
            interval_pre = find(tt<0);
            freq_search = find(frq>frequency_range);
        end
    
        xc_pre = abs(cfs(freq_search,interval_pre));
        vtmp = mean(xc_pre(:))+norminv(1-risk,0,1)*std(xc_pre(:));
        v = [vtmp,vtmp];
    
        [M{i},c] = contour3(tt(interval),frq(freq_search),abs(cfs(freq_search,interval)),v,'r-');
    
        subplot(nplots,2,k2);

        if isempty(M{i})
           plot(tt, htmp, 'color', 'r')
        else
            plot(tt, htmp, 'color', 'g')
        end
        axis tight;
        xlabel("Time (s)"); ylabel("Amplitude")
        
    end
    
    if strcmp(system,'CERIAH')
        idx = find(cellfun('isempty', M));
        thrsh = idx;
    else
        try
            idx = find(cellfun('isempty', M),1);
            thrsh = L(idx-1);
        catch
            idx = find(cellfun('isempty', M),length(M));
            thrsh = L(idx(end)+1);
        end
        disp(['Threshold is ' num2str(thrsh) 'dB'])
    end

    output = thrsh;
    
end
