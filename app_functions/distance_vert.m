function c = distance_vert(x1,x2)

    c = sqrt((x2-x1)^2);

end
