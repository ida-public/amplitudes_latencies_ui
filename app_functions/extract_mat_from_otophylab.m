function new_mat_file = extract_mat_from_otophylab(xlsfile)

    [A] = importdata(xlsfile);
    data = A.data.ABR0xB5Volts; 
    
    L = [];
    timedata=[];
    h = [];
    
    for i = 1:2:size(data,2)
        L = [L data(1,i)];
        timedata = [timedata data(7:end,i)]; 
        h = [h data(7:end,i+1)];
    end
    
    
    dt = timedata(2,1)- timedata(1,1); 
    d = A.textdata.ABR0xB5Volts{5,2}; % Stim freq
    id1 = strfind(d,'Hz');
    fmin = str2double(d(1:id1-1));
    
    stimparams.system = 'Otophylab'; 
    stimparams.Ltube = '0';
    stimparams.fech = 1./dt; %kHz
    stimparams.gain = 1e6; %because it is already included in the read_data so I just convert them in V.
    stimparams.espace = 0;
    stimparams.Lmin=min(L);
    stimparams.Lmax=max(L);
    stimparams.L=L;
    stimparams.delL=abs(L(2)-L(1));
    stimparams.fmin=fmin;
    stimparams.fmax=fmin;
    stimparams.delf=0;
    stimparams.stim_type=1;
    
    hrav = h;
    tt1 = timedata(:,1)*1e-3;
    new_mat_file.tt1 = tt1; 
    new_mat_file.hrav = hrav; 
    new_mat_file.stimparams = stimparams; 

    save([xlsfile(1:end-4) '.mat'], 'tt1','hrav','stimparams');

end
