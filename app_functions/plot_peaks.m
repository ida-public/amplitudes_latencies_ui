function plot_peaks(fig,data,tt,sigma0,dir_ref_pos,app)

    hf = data;
    hold(fig, 'on')
    colorpalette = ['c', 'r', 'g', 'b', 'm'];
    nwaves = 5;
    reverse = app.Reverse.Value;
    cr1 = app.Wave1ms.Value;

    %% This section is to add a new constraint
    % when decreasing intensity, latencies are longer and longer. 
    % Therefore, we want to impose that wave 1 is shifted on the time axis.
    try 
        data_tmp = load(dir_ref_pos.name); 
        if app.N1infP1CheckBox.Value
            pos_ref = data_tmp.save_roi{1,2}(1); % N1
        else
            pos_ref = data_tmp.save_roi{1,1}(1); % P1
        end
    catch
        pos_ref = 0;
    end

    % On s'intéresse à une fenêtre de [0-10]ms après la stim onset
    if pos_ref>0
        interval = find(tt>(pos_ref-0.1) & tt<10); %based on result already validated 
    else
        interval = find(tt>cr1 & tt<10); %default
    end

    % initialisation des pic / nodes
    pks = 0.2*ones(1,nwaves); 
    locs_pks = 2*(1:nwaves);
    nodes = zeros(1,nwaves);
    locs_nodes = 2*(1:nwaves);

    [pks_tmp,locs_pks_tmp] = findpeaks(hf(interval),'MinPeakProminence',sigma0);
    [nodes_tmp,locs_nodes_tmp] = findpeaks(-hf(interval),'MinPeakProminence',sigma0);

    if ~isempty(nodes_tmp) && ~isempty(pks_tmp)
        if app.N1infP1CheckBox.Value && locs_pks_tmp(1) < locs_nodes_tmp(1) % il faut que N1<P1
            pks(1:min(length(pks_tmp), nwaves)) = pks_tmp(1:min(length(pks_tmp), nwaves));
            locs_pks(1:min(length(pks_tmp), nwaves)) = locs_pks_tmp(1:min(length(pks_tmp), nwaves));
            nodes_tmp1 = [0; nodes_tmp(1:min(length(nodes_tmp), nwaves-1))];
            locs_nodes_tmp1 = [2; locs_nodes_tmp(1:min(length(locs_nodes_tmp), nwaves-1))];
            nodes(1:min(length(locs_nodes_tmp1), nwaves)) = nodes_tmp1; 
            locs_nodes(1:min(length(locs_nodes_tmp1), nwaves)) = locs_nodes_tmp1; 
        elseif app.P1infN1CheckBox.Value && locs_nodes_tmp(1) < locs_pks_tmp(1) % il faut que P1<N1
            nodes(1:min(length(nodes_tmp), nwaves)) = nodes_tmp(1:min(length(nodes_tmp), nwaves));
            locs_nodes(1:min(length(locs_nodes_tmp), nwaves)) = locs_nodes_tmp(1:min(length(locs_nodes_tmp), nwaves));
            pks_tmp1 = [0; pks_tmp(1:min(length(pks_tmp), nwaves-1))];
            locs_pks_tmp1 = [2; locs_pks_tmp(1:min(length(locs_pks_tmp), nwaves-1))];
            pks(1:min(length(pks_tmp1), nwaves)) = pks_tmp1; 
            locs_pks(1:min(length(pks_tmp1), nwaves)) = locs_pks_tmp1;
        else
            pks(1:min(length(pks_tmp), nwaves)) = pks_tmp(1:min(length(pks_tmp), nwaves));
            locs_pks(1:min(length(pks_tmp), nwaves)) = locs_pks_tmp(1:min(length(pks_tmp), nwaves));
            nodes(1:min(length(nodes_tmp), nwaves)) = nodes_tmp(1:min(length(nodes_tmp), nwaves));
            locs_nodes(1:min(length(locs_nodes_tmp), nwaves)) = locs_nodes_tmp(1:min(length(locs_nodes_tmp), nwaves));
        end
    end

    xx_peaks = tt(interval(locs_pks)); 
    yy_peaks = pks;
    xx_nodes = tt(interval(locs_nodes)); 
    yy_nodes = -nodes;

    
    for i = 1:min(length(pks), length(nodes))

        nroi = drawpoint(fig,'Position',[xx_nodes(i) yy_nodes(i)], 'Color', colorpalette(i), 'Label', ['N' num2str(i)], 'Tag', ['N' num2str(i)], 'LabelAlpha', 0.2);
        addlistener(nroi,'ROIMoved',@allevents);
       
        proi = drawpoint(fig,'Position',[xx_peaks(i) yy_peaks(i)], 'Color', colorpalette(i), 'Label', ['P' num2str(i)], 'Tag', ['P' num2str(i)],'LabelAlpha', 0.2);
        addlistener(proi,'ROIMoved',@allevents);
    end


    function allevents(~,evt)
        disp(['ROI moved Previous Position: ' mat2str(evt.PreviousPosition)]);
        disp(['ROI moved Current Position: ' mat2str(evt.CurrentPosition)]);
    end

end